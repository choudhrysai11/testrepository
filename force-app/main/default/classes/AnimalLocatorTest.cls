@isTest
Private class AnimalLocatorTest
{
    @isTest static void testCallout() {
    // Set mock callout class 
    Test.setMock(HttpCalloutMock.class, new AnimalLocatorMock()); 
    // This causes a fake response to be sent
    // from the class that implements HttpCalloutMock. 
    string response = AnimalLocator.getAnimalNameById(1);
    // Verify that the response received contains fake values
    String actualValue = response;
    String expectedValue = 'name';
    System.assertEquals(actualValue, expectedValue);
}
}