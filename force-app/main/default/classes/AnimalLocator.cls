public class AnimalLocator
{
    public static string getAnimalNameById(integer ID)
    {
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        string endpt='https://th-apex-http-callout.herokuapp.com/animals/'+ID;
        request.setEndpoint(endpt);
        request.setMethod('GET');
        string name='';
        HttpResponse response = http.send(request);
        // If the request is successful, parse the JSON response.
        if (response.getStatusCode() == 200) {
            JSONParser parser = JSON.createParser(response.getBody());
            while (parser.nextToken() != null) {
                if(parser.getCurrentName() == 'name') {
                    parser.nextValue();
                    name=parser.getText();
                }
            }
            
        }
        return name;

    }
}