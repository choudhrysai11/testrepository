public class MWT_AccountApex {
    
    @AuraEnabled
    public static account saveAccount (Account ac)
    {
        upsert ac;
        return ac;
    }
    @AuraEnabled
     public static list<account> fetchAccounts()
    {
        return [select id,name,annualrevenue,Completed__c from account];
    }

}