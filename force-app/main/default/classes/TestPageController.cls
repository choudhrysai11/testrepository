public class TestPageController {   

    public TestPageController(){

    }

    public List<gaugeData> getData() {
        double NPS_score = 0;
        integer score = [Select count()  From case Where score__c!=null]; 

        List<gaugeData> data = new List<gaugeData>();
        data.add(new gaugeData('NPS', score));
        return data;
    }
    public class gaugeData {
        public String name { get; set; }
        public decimal score { get; set; }

        public gaugeData(String name, decimal npsScore) {
            this.name = name;
            this.score = npsScore;
        }
    }
}