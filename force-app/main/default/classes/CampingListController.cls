public class CampingListController {
    
    @AuraEnabled
    public static List<Camping_item__c> getItems()
    {
        return [select id,Name,Price__c,Quantity__c,Packed__c from Camping_item__c limit 1000];
    }
    @AuraEnabled
    public static Camping_item__c saveItem (Camping_item__c camp)
    {
        upsert camp;
        return camp;
    }

}