public class skillContoller {

    public String departmentoption { get; set; }

    public List<fieldset> que { get; set; }
    
    public string department{get; set;}
    public string division{get; set;}
    public string subdivision{get; set;}
    public string language{get; set;}
    public string network{get; set;}
    
    public List<SelectOption> departmentList{get;set;}
    public List<SelectOption> divisionList{get;set;}
    public List<SelectOption> subdivisionList{get;set;}
    public List<SelectOption> languageList{get;set;}
    public List<SelectOption> networkList{get;set;}
    public List<user> userList{get;set;}
    
    public Boolean MoveTo{get;set;}
    

    public skillContoller()
    {
         List<SelectOption> department= new List<SelectOption>();
         Schema.DescribeFieldResult departmentResult= OmniChannel_Skills__c.Department__c.getDescribe();
         List<Schema.PicklistEntry> departmentpicklist= departmentResult.getPicklistValues();
         for( Schema.PicklistEntry f : departmentpicklist){
            department.add(new SelectOption(f.getValue(), f.getValue()));}
         
         This.departmentList=department;
         
         List<SelectOption> division= new List<SelectOption>();
         Schema.DescribeFieldResult divisionresult= OmniChannel_Skills__c.Division__c.getDescribe();
         List<Schema.PicklistEntry> divisionpicklist= divisionresult.getPicklistValues();
         for( Schema.PicklistEntry f : divisionpicklist){
            division.add(new SelectOption(f.getValue(), f.getValue()));}
         
         This.divisionlist=division;
         
         List<SelectOption> subdivision= new List<SelectOption>();
         Schema.DescribeFieldResult subdivisionresult= OmniChannel_Skills__c.Sub_Division__c.getDescribe();
         List<Schema.PicklistEntry> subdivisionpicklist= subdivisionresult.getPicklistValues();
         for( Schema.PicklistEntry f : subdivisionpicklist){
            subdivision.add(new SelectOption(f.getValue(), f.getValue()));}
         
         This.subdivisionList=subdivision;
         
         List<SelectOption> language= new List<SelectOption>();
         Schema.DescribeFieldResult languageresult= OmniChannel_Skills__c.Language__c.getDescribe();
         List<Schema.PicklistEntry> lanuagepicklist= languageresult.getPicklistValues();
         for( Schema.PicklistEntry f : lanuagepicklist){
            language.add(new SelectOption(f.getValue(), f.getValue()));}
         
         This.languageList=language;
         
         List<SelectOption> network= new List<SelectOption>();
         Schema.DescribeFieldResult networkresult= OmniChannel_Skills__c.Social_Network__c.getDescribe();
         List<Schema.PicklistEntry> networkpicklist= networkresult.getPicklistValues();
         for( Schema.PicklistEntry f : networkpicklist){
            network.add(new SelectOption(f.getValue(), f.getValue()));}
         
         This.networkList=network;
    }
    public PageReference runSearch() {
        
        List<OmniChannel_Skills__c > skillList= new List<OmniChannel_Skills__c >();
        list<id> userid= new List<id>();
        skillList= [SELECT Agent_Name__c FROM OmniChannel_Skills__c where Department__c=:department and  Division__c=:division and Sub_Division__c=:subdivision and language__c includes (:language) and Social_Network__c includes (:network) ];
        system.debug(skillList);
        for(OmniChannel_Skills__c omn: skillList)
        {
            userid.add(omn.Agent_Name__c );
        }
        system.debug(userid);
        userList= [Select id,Name from User where id IN :userid Limit 500];
        system.debug(userList);
        return null;
    }
    
    public PageReference MoveToButton(){
    
           system.debug('Inside Move to Button') ;
        MoveTo=True;
        system.debug(MoveTo);
        return null;
    }
}