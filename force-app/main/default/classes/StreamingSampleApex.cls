public class StreamingSampleApex {
    
    @AuraEnabled
    public static string getUserSession() {
        return userInfo.getSessionId();
    }
}