({
    handleShowPopover : function(component, event, helper) {
        component.set("v.showit",true);
        setTimeout(function(){ 
                //close the popover after 3 seconds
                component.set("v.showit",false);
            }, 3000);
    }
})