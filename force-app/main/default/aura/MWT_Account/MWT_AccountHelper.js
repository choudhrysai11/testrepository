({
    getAccounts : function(component)
    {
        var action=component.get("c.fetchAccounts");
        action.setCallback(this,function(response) {
            var state= response.getState();
            if(state==="SUCCESS")
            {
                
                component.set("v.accountList",response.getReturnValue());
            }
            else
            {
                console.log("An error has occured");
            }
        });
        $A.enqueueAction(action);
    },
    createAccount : function(component,evtAcc) {
        var action=component.get("c.saveAccount");
        action.setParams(
            {
                "ac":evtAcc
            }
        );
        action.setCallback(this,function(response) {
            var state= response.getState();
            if(component.isValid() && state==="SUCCESS")
            {
                var resultToast=$A.get("e.force:showToast");
                resultToast.setParams({
                    "title":"Account Saved",
                    "message":"the new Account was created and added to list"
                });
                
                var accounts=component.get("v.accountList");
                accounts.push(response.getReturnValue());
                component.set("v.accountList",accounts);
                resultToast.fire();
                
            }
            else
            {
                console.log("An error has occured");
            }
        });
        $A.enqueueAction(action);
	}
})