({
    startListening: function (component, event, helper) {
        //Get a valid Session Id
        var sessionAction = component.get("c.getUserSession");
        
        sessionAction.setCallback(this, function (a) {
            var sid = a.getReturnValue();
            var channel = '/topic/InvoiceStatementUpdates';
            var cometd =  $.cometd;
            var subscribedToChannel;
            var isExtensionEnabled;
            var metaConnectListener;
            var metaDisconnectListener;
            var metaHandshakeListener;
            var metaSubscribeListener;
            var metaUnSubscribeListener
            var metaUnSucessfulListener;
            
            var _connected = false;
            if(!metaConnectListener) {
                metaConnectListener = cometd.addListener('/meta/connect', function(message) {        
                    if (cometd.isDisconnected()) {
                        console.log('Disconnected: '+JSON.stringify(message));
                        return;
                    }
                    
                    var wasConnected = _connected;                
                    _connected = message.successful;
                    
                    if (!wasConnected && _connected) {
                        console.log('DEBUG: Connection Successful : '+JSON.stringify(message));                    
                    } else if (wasConnected && !_connected) {
                        console.log('DEBUG: Disconnected from the server'+JSON.stringify(message));
                    }
                }); 
            }   
            
            if(!metaDisconnectListener) {
                metaDisconnectListener = cometd.addListener('/meta/disconnect', function(message) {  
                    console.log('DEBUG: /meta/disconnect message: '+JSON.stringify(message));
                });
            }
            if(!metaHandshakeListener) {
                metaHandshakeListener = cometd.addListener('/meta/handshake', function(message) {
                    if (message.successful) {
                        console.log(' DEBUG: Handshake Successful: '+JSON.stringify(message));                            
                        
                        if (message.ext && message.ext[REPLAY_FROM_KEY] == true) {
                            isExtensionEnabled = true;
                        }                    
                        subscribedToChannel = subscribe(channel);
                    } else
                        console.log('DEBUG: Handshake Unsuccessful: '+JSON.stringify(message));
                });
            }
            
            if(!metaSubscribeListener) {
                var cometd =  $.cometd;
                metaSubscribeListener = cometd.addListener('/meta/subscribe', function(message) {  
                    if (message.successful) {
                        console.log('DEBUG: Subscribe Successful '+channel+': '+JSON.stringify(message));
                    } else {
                        console.log('DEBUG: Subscribe Unsuccessful '+channel+': '+JSON.stringify(message));                
                    }    
                });
            }
            
            
            if(!metaUnSubscribeListener) {
                var cometd =  $.cometd;
                metaUnSubscribeListener = cometd.addListener('/meta/unsubscribe', function(message) {  
                    if (message.successful) {
                        console.log('DEBUG: Unsubscribe Successful '+JSON.stringify(message));
                    } else {
                        console.log('DEBUG: Unsubscribe Unsuccessful '+JSON.stringify(message));                
                    }
                });    
            }                
            
            // notifies any failures
            if(!metaUnSucessfulListener) {
                var cometd =  $.cometd;
                metaUnSucessfulListener = cometd.addListener('/meta/unsuccessful', function(message) {  
                    console.log('DEBUG:/meta/unsuccessful Error: '+JSON.stringify(message));
                });
            }
            
            cometd.websocketEnabled = false;
            cometd.configure({
                url:'/cometd/28.0/',
                requestHeaders: { Authorization: 'OAuth '+sid}
            });
            cometd.handshake();
            
            function subscribe(channel) {
                // Subscribe to a topic. JSON-encoded update will be returned in the callback
                return cometd.subscribe(channel,  $A.getCallback(function (message) {
                    console.log('in subscribe');
                    /*var e=new Notification("Case : " + '123456',{
                        body:"New message attached to the current case"
                    });*/
                    
                }));
            }
            function disconnect() {
                if (cometd) {
                    cometd.removeListener(metaConnectListener);
                    cometd.removeListener(metaDisconnectListener);
                    cometd.removeListener(metaHandshakeListener);
                    cometd.removeListener(metaSubscribeListener);
                    cometd.removeListener(metaUnSubscribeListener);
                    cometd.unsubscribe(subscribedToChannel);
                    cometd.disconnect();
                }
            }    
            window.onbeforeunload = disconnect;
        });
        $A.enqueueAction(sessionAction);
    },
    handleShowNotificationEvent: function (component, event, helper) {
        
        var data = event.getParam('data');              
        
    }
})