({
	createAccount : function(component) {
		var accnt= component.get('v.acc');
        var evt=component.getEvent("createAccount");
        evt.setParams(
            {
                "eventAccount":accnt
            }
        );
        evt.fire();
        component.set("v.acc",{
            'sObjectType':'',
            'Name':'',
            'AnnualRevenue':0
        });
	}
})