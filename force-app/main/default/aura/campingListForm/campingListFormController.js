({
    clickCreateItem : function(component, event, helper)
    {
        if(component.isValid())
        {
            var validItem = component.find('cmpForm').reduce(function (validSoFar, inputCmp) {
                // Displays error messages for invalid fields
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && inputCmp.get('v.validity').valid;
            }, true);      
            if(validItem)
            {
                helper.createItem(component);
            }
        }
    }
    
})