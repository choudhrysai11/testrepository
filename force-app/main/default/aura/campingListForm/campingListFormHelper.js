({
    createItem : function(component) {
        var cmp=component.get("v.newItem");
        var evt=component.getEvent("addItem");
        evt.setParams({
            "item":cmp
        });
        evt.fire();
        component.set("v.newItem",{'sobjectType': 'Camping_item__c',
                                   'Name':'',
                                   'Price__c': 0,
                                   'Quantity__c': 0,
                                   'Packed__c': false
                                  });
    }
 })