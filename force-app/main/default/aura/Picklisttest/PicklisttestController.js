({
 
	doInit: function(component, event, helper) {
		//call the helper function with pass [component, Controller field and Dependent Field] Api name 
		helper.fetchPicklistValues(component, 'Picklist_One__c', 'picklist_2__c');
		helper.fetchPicklistValues2(component, 'picklist_2__c', 'picklist_3__c');
	},
 
 
	// function call on change tha controller field  
	onControllerFieldChange: function(component, event, helper) {
		alert(event.getSource().get("v.value"));
		// get the selected value
		var controllerValueKey = event.getSource().get("v.value");
        var Map;
        alert(component.get("v.isDependent1Disable"));
        alert(component.get("v.isDependent2Disable"));
 		if(component.get("v.isDependent1Disable"))
        {
            alert("Inside First");
            Map = component.get("v.depnedentFieldMap1");
        }
        else if(component.get("v.isDependent2Disable"))
        {
            alert("Inside Second");
            Map = component.get("v.depnedentFieldMap2");
        }
        alert(Map);
		// check if selected value is not equal to None then call the helper function.
		// if controller field value is none then make dependent field value is none and disable field
		if (controllerValueKey != '--- None ---') {
 			alert("inside controllerValueKey");
			// get dependent values for controller field by using map[key].  
			// for i.e "India" is controllerValueKey so in the map give key Name for get map values like 
			// map['India'] = its return all dependent picklist values.
			var ListOfDependentFields = Map[controllerValueKey];
			alert(ListOfDependentFields);
			helper.fetchDepValues(component, ListOfDependentFields);
		}
		else {
			var defaultVal = [{
				class: "optionClass",
				label: '--- None ---',
				value: '--- None ---'
			}];
			component.find('conState').set("v.options", defaultVal);
			component.set("v.isDependentDisable", true);
		}
	},
 
	// function call on change tha Dependent field    
	onDependentFieldChange: function(component, event, helper) {
		alert(event.getSource().get("v.value"));
	}
})