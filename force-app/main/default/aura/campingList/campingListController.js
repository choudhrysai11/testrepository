({
    doInit : function(component, event, helper)
    {
        var action=component.get("c.getItems");
        action.setCallback(this,function(response) {
            var state= response.getState();
            if(state==="SUCCESS")
            {
                
                component.set("v.items",response.getReturnValue());
            }
            else
            {
                console.log("An error has occured");
            }
        });
        $A.enqueueAction(action);
    },
    handleAddItem :function(component, event, helper)
    {
        var evntcamp= event.getParam("item");
        var action=component.get("c.saveItem");
        var cmplid= component.get("v.items");
        action.setParams({"camp":evntcamp});
        action.setCallback(this,function(response) {
            var state= response.getState();
            if(state==="SUCCESS")
            {
                cmplid.push(response.getReturnValue());
                component.set("v.items",cmplid);
                component.set("v.newItem",{'sObjectType':'Camping_item__c',
                                           'Name':'',
                                           'Price__c': 0,
                                           'Quantity__c': 0,
                                           'Packed__c': false
                                          });
            }
            else
            {
                console.log("An error has occured");
            }
        });
        $A.enqueueAction(action);   
    }

})